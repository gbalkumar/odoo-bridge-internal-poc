var express = require('express');
var router = express.Router();
var util = require('util');

/* Helpers */
var constants = require('../helpers/constants');
var paramHelper = require('../helpers/param');
var authCheckHelper = require('../helpers/authcheck');
var jaysonHelper = require('../helpers/jayson');
var modelHelper = require('../helpers/model');
var map = require('../helpers/hashmap');
var appDebug = require('debug')('odoo-bridge:app');
var debug = require('debug')('odoo-bridge:model');

router.use(authCheckHelper.authSessionCheck);

var errorHandler = function (error, response) {
  var errorJSON = constants.ERRORS.JAYSON_ERROR;
  errorJSON['message'] = error.toString();
  return response.status(500).json(errorJSON);
};

var isObject = function (a) {
  return (!!a) && (a.constructor === Object);
};

/* Router methods start here */

router.get('/:modelName', function (req, res, next) {

  var idToken = req.header(constants.XAUTH_HEADERS.ID_TOKEN);
  var accessToken = req.header(constants.XAUTH_HEADERS.ACCESS_TOKEN);
  var sessionId = req.header(constants.XAUTH_HEADERS.SESSION_ID);

  var params = modelHelper.constructParamObject(req);
  debug(params);

  var client = jaysonHelper.getJaysonClient(idToken, sessionId, constants.URL_PATH_GET);

  jaysonHelper.callJaysonRequest(client, params, function (error, result) {
    if (error) {
      return errorHandler(error, res);
    }
    res.status(200).json(result);
  });

});

router.get('/:modelName/:recordID', function (req, res, next) {

  var idToken = req.header(constants.XAUTH_HEADERS.ID_TOKEN);
  var accessToken = req.header(constants.XAUTH_HEADERS.ACCESS_TOKEN);
  var sessionId = req.header(constants.XAUTH_HEADERS.SESSION_ID);

  var modelName = paramHelper.getParamValue(req, 'modelName');
  var recordID = paramHelper.getParamValue(req, 'recordID');
  if (isNaN(recordID)) {
    res.status(500).json(constants.ERRORS.INVALID_RECORD_ID);
    return;
  }

  recordID = parseInt(recordID);

  var params = modelHelper.constructParamObject(req, [
    ['id', '=', recordID]
  ]);

  debug(params);

  var client = jaysonHelper.getJaysonClient(idToken, sessionId, constants.URL_PATH_GET);

  jaysonHelper.callJaysonRequest(client, params, function (error, result) {
    res.status(200).json(result);
  });

});

router.post('/:modelName', function (req, res, next) {

  var idToken = req.header(constants.XAUTH_HEADERS.ID_TOKEN);
  var accessToken = req.header(constants.XAUTH_HEADERS.ACCESS_TOKEN);
  var sessionId = req.header(constants.XAUTH_HEADERS.SESSION_ID);

  var sessionContextUUID = authCheckHelper.uuidForContext(idToken, accessToken);
  var sessionContext = map.get(sessionContextUUID);
  var modelName = paramHelper.getParamValue(req, 'modelName');
  var fields = paramHelper.getRequestBodyContent(req);
  if (isObject(fields) === false) {
    return errorHandler('Invalid Body Structure; should be an object', res);
  }
  var params = modelHelper.createAndUpdateParameterTemplate();

  params.model = modelName;
  params.method = 'create';
  params.args = [fields];
  params.kwargs.context = sessionContext.user_context;

  debug(params);

  var client = jaysonHelper.getJaysonClient(idToken, sessionId, util.format(constants.URL_PATH_POST, modelName));

  jaysonHelper.callJaysonRequest(client, params, function (error, result) {
    if (error) {
      return errorHandler(error, res);
    }
    res.status(200).json({
      result: true,
      id: result
    });
  });

});

router.patch('/:modelName/:recordID', function (req, res, next) {
  // get the cookie values
  var idToken = req.header(constants.XAUTH_HEADERS.ID_TOKEN);
  var accessToken = req.header(constants.XAUTH_HEADERS.ACCESS_TOKEN);
  var sessionId = req.header(constants.XAUTH_HEADERS.SESSION_ID);

  var sessionContextUUID = authCheckHelper.uuidForContext(idToken, accessToken);
  var sessionContext = map.get(sessionContextUUID);
  var modelName = paramHelper.getParamValue(req, 'modelName');

  var recordID = paramHelper.getParamValue(req, 'recordID');
  if (isNaN(recordID)) {
    res.status(500).json(constants.ERRORS.INVALID_RECORD_ID);
    return;
  }

  recordID = parseInt(recordID);

  var fields = paramHelper.getRequestBodyContent(req);
  if (isObject(fields) === false) {
    return errorHandler('Invalid Body Structure; should be an object', res);
  }
  var params = modelHelper.createAndUpdateParameterTemplate();

  params.model = modelName;
  params.method = 'write';
  params.args.push([recordID], fields);
  params.kwargs.context = sessionContext.user_context;

  debug(params);

  var client = jaysonHelper.getJaysonClient(idToken, sessionId, constants.URL_PATH_PATCH);

  jaysonHelper.callJaysonRequest(client, params, function (error, result) {
    if (error) {
      return errorHandler(error, res);
    }
    debug('result of update operation: ' + result);
    res.status(200).json({
      result: result,
      message: 'Update Successful'
    });
  });

});

router.delete('/:modelName/:recordID', function (req, res, next) {

  var idToken = req.header(constants.XAUTH_HEADERS.ID_TOKEN);
  var accessToken = req.header(constants.XAUTH_HEADERS.ACCESS_TOKEN);
  var sessionId = req.header(constants.XAUTH_HEADERS.SESSION_ID);

  var sessionContextUUID = authCheckHelper.uuidForContext(idToken, accessToken);
  var sessionContext = map.get(sessionContextUUID);
  var modelName = paramHelper.getParamValue(req, 'modelName');

  var recordID = paramHelper.getParamValue(req, 'recordID');
  if (isNaN(recordID)) {
    res.status(500).json(constants.ERRORS.INVALID_RECORD_ID);
    return;
  }

  recordID = parseInt(recordID);
  var params = modelHelper.createAndUpdateParameterTemplate();

  params.model = modelName;
  params.method = 'unlink';
  params.args.push([recordID]);
  params.kwargs.context = sessionContext.user_context;

  debug(params);

  var client = jaysonHelper.getJaysonClient(idToken, sessionId, constants.URL_PATH_DELETE);

  jaysonHelper.callJaysonRequest(client, params, function (error, result) {
    if (error) {
      errorHandler(error, res);
      return;
    }
    res.status(200).json({
      result: result,
      message: 'Deletion Successful'
    });
  });

});

module.exports = router;
