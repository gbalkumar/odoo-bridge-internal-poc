var express = require('express');
var router = express.Router();
var paramHelper = require('../helpers/param');
var authCheckHelper = require('../helpers/authcheck');
var actionHelper = require('../helpers/action');
var jaysonHelper = require('../helpers/jayson');

router.use(authCheckHelper.authSessionCheck);

router.post('/:modelName/:actionName', function (req, res, next) {

  var modelName = paramHelper.getParamValue(req, 'modelName');
  var actionName = paramHelper.getParamValue(req, 'actionName');

  actionHelper.invokeAction(req, modelName, actionName, function (error, result) {
    if (error) {
      (error === 'Not Found') ? res.status(404) : res.status(500);
      res.json(jaysonHelper.getErrorObject(error));
    } else {
      res.status(200).json({
        result: true,
        response: result
      });
    }
  });

});

module.exports = router;
