var express = require('express');
var router = express.Router();
var request = require('request');
var jayson = require('jayson');
var util = require('util');

/* Debugging */
var debug = require('debug')('odoo-bridge:auth');
require('request-debug')(request);

/* Helpers */
var constants = require('../helpers/constants');
var jaysonHelper = require('../helpers/jayson');
var authCheckHelper = require('../helpers/authcheck');
var authHelper = require('../helpers/auth');
var map = require('../helpers/hashmap');

/* Routing starts here */
router.use(authCheckHelper.authSessionCheck);

router.get('/get_session_info', function (req, res, next) {

  var idToken = req.header(constants.XAUTH_HEADERS.ID_TOKEN);
  var accessToken = req.header(constants.XAUTH_HEADERS.ACCESS_TOKEN);

  var keyName = authCheckHelper.uuidForContext(idToken, accessToken);
  var context = map.get(keyName);

  if (context) {
    res.status(200).json(context);
  } else {
    res.status(500).json(constants.ERRORS.SERVER_ERROR);
  }

});

module.exports = router;
