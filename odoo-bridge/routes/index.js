var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {
    title: 'Odoo Bridge API v1'
  });
});

module.exports = router;
