var constants = require('./constants');
var Cookie = require('tough-cookie').Cookie;

var CookieHelper = {

  parseSessionCookie: function(responseHeader) {
    if (responseHeader['set-cookie'] === undefined) {
      return false;
    }

    if (responseHeader['set-cookie'] instanceof Array && responseHeader['set-cookie'].length === 0) {
      return false;
    }

    var cookies, session_id;
    if (responseHeader['set-cookie'] instanceof Array) {
      cookies = responseHeader['set-cookie'].map(Cookie.parse);
    } else {
      cookies = [Cookie.parse(res.headers['set-cookie'])];
    }

    // iterating through the available cookies
    cookies.every(function(cookieObject) {
      if (cookieObject.key === constants.COOKIES.SESSION_ID) {
        session_id = cookieObject.value;
        // break the loop
        return false;
      }
      // proceed with next iteration
      return true;
    });

    return (session_id !== undefined && session_id.trim().length !== 0) ? session_id : false;
  }
};

module.exports = CookieHelper;
