
var paramHelper = require('./param');
var defined = require('./undefined').defined;
var debug = require('debug')('odoo-bridge:model');

var ModelHelper = {
  getParameterTemplate: function () {
    return {
      "model": '',
      "fields": [

      ],
      "domain": [

      ],
      "context": {
        "lang": "en_US",
        "tz": false
      },
      "offset": 0,
      "limit": 40,
      "sort": ""
    };
  },

  createAndUpdateParameterTemplate: function () {
    return {
      "model": "",
      "method": "",
      "args": [],
      "kwargs": {
        "context": {}
      }
    };
  },

  constructParamObject: function (request, customFilters) {
    var _self = this;
    var modelName = paramHelper.getParamValue(request, 'modelName');
    var fields = paramHelper.getQueryValue(request, 'fields');
    var filters = paramHelper.getQueryValue(request, 'filters');

    var params = _self.getParameterTemplate();
    params.model = modelName;
    if (defined(fields)) {
      params.fields = fields.split(',');
    }

    params.fields.push('create_date', 'create_uid', '__last_update');

    debug(filters);
    if (defined(filters)) {
      var domain = JSON.parse(filters.replace(/'/g, '"'));
      params.domain = defined(domain) ? domain : [];
    }

    if (defined(customFilters) && customFilters.length > 0) {
      customFilters.forEach(function (customFilter) {
        params.domain.push(customFilter);
      });
    }

    if (defined(request.query.offset) && paramHelper.getIntegerQueryValue(request, 'offset') !== NaN) {
      params.offset = paramHelper.getIntegerQueryValue(request, 'offset');
    }
    if (defined(request.query.limit) && paramHelper.getIntegerQueryValue(request, 'limit') !== NaN) {
      params.limit = paramHelper.getIntegerQueryValue(request, 'limit');
    }

    return params;
  }
};

module.exports = ModelHelper;
