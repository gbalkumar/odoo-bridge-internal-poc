var debug = require('debug')('odoo-bridge:actionHelper');
var constants = require('./constants');
var jaysonHelper = require('./jayson');
var paramHelper = require('./param');
var util = require('util');

var ActionHelper = {
  invokeAction: function (req, modelName, actionName, resultCallback) {

    var idToken = req.header(constants.XAUTH_HEADERS.ID_TOKEN);
    var accessToken = req.header(constants.XAUTH_HEADERS.ACCESS_TOKEN);
    var sessionId = req.header(constants.XAUTH_HEADERS.SESSION_ID);

    var requestUrl = util.format(constants.URL_INVOKE_ACTION_DIRECT, modelName, actionName);
    var client = jaysonHelper.getJaysonClient(idToken, sessionId, requestUrl);
    var params = paramHelper.getRequestBodyContent(req);
    debug(params);

    jaysonHelper.callJaysonRequest(client, params, function (error, result) {
      return resultCallback(error, result);
    });

  }
};

module.exports = ActionHelper;
