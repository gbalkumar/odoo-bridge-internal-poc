/* Helpers */
var util = require('util');
var map = require('./hashmap');
var constants = require('./constants');
var authRegisterHelper = require('./auth');
var jaysonHelper = require('./jayson');
var appDebug = require('debug')('odoo-bridge:app');
var debug = require('debug')('odoo-bridge:authCheckHelper');
var defined = require('./undefined').defined;
var notEmpty = require('./undefined').notEmpty;

var headersPresentAndValid = function (req) {
  return notEmpty(req.header(constants.XAUTH_HEADERS.ID_TOKEN)) &&
    notEmpty(req.header(constants.XAUTH_HEADERS.ACCESS_TOKEN));
};

var uuid = function (id_token, access_token) {
  return util.format('%s;%s', id_token, access_token);
};

var uuidForContext = function (id_token, access_token) {
  return util.format('%s;%s;context', id_token, access_token);
};

var authSessionCheck = function (req, res, next) {
  // get the header values
  var idToken = req.header(constants.XAUTH_HEADERS.ID_TOKEN);
  var accessToken = req.header(constants.XAUTH_HEADERS.ACCESS_TOKEN);

  if (!headersPresentAndValid(req)) {
    return res.status(500).json(constants.ERRORS.TOKENS_NOT_SET);
  }

  var sessionKeyName = uuid(idToken, accessToken);

  if (map.has(sessionKeyName)) {
    appDebug('Session is already registered...');
    debug('session_id: ' + map.get(sessionKeyName));
    req.headers[constants.XAUTH_HEADERS.SESSION_ID] = map.get(sessionKeyName);
    return next();
  } else {
    // login and set context
    appDebug('No session set. Registering the session...');
    authRegisterHelper.getSessionId(idToken, accessToken, function (error, sessionId) {
      if (error) {
        return res.status(500).json(jaysonHelper.getErrorObject('Cannot get session_id'));
      }
      authRegisterHelper.getSessionInfo(idToken, sessionId, function (error, sessionContext) {
        if (error) {
          return res.status(500).json(jaysonHelper.getErrorObject('Cannot get session_context'));
        } else if (sessionContext.uid === null && sessionContext.username === null) {
          return res.status(401).json(constants.ERRORS.INVALID_TOKENS);
        }
        var sessionContextKeyName =  uuidForContext(idToken, accessToken);
        map.set(sessionKeyName, sessionId);
        map.set(sessionContextKeyName, sessionContext);
        debug(map.get(sessionContextKeyName));
        appDebug('Session Registered.');
        req.headers[constants.XAUTH_HEADERS.SESSION_ID] = sessionId;
        return next();
      });
    });
  }
};

module.exports = {
  headersPresentAndValid: headersPresentAndValid,
  authSessionCheck: authSessionCheck,
  uuid: uuid,
  uuidForContext: uuidForContext
};
