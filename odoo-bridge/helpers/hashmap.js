var LRU = require("lru-cache")
var options = {
  max: 10,
  maxAge: 1000 * 60 * 10
};
var cache = LRU(options);

module.exports = cache;
