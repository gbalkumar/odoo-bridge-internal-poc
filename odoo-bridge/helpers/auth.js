/* Helper file for routes/auth.js */
var util = require('util');
var request = require('request');
require('request-debug')(request);

var cookieHelper = require('./cookie');
var constants = require('./constants');
var jaysonHelper = require('./jayson');

var AuthHelper = {
  /* Session ID related functions */
  sessionIdResponseHandler: function (error, response, resultCallback) {
    if (!error && response.statusCode === 303) {
      var session_id = cookieHelper.parseSessionCookie(response.headers);
      return resultCallback(null, session_id);
    } else {
      // throw error
      debug('error getting session_id');
      return resultCallback(error);
    }
  },

  getSessionId: function (id_token, access_token, resultCallback) {
    var _self = this;
    var odoo_oauth_url = util.format(constants.URL_ODOO_OAUTH_EP, id_token, access_token);
    var cookieString = util.format('id_token=%s', id_token);

    var cookieJar = request.jar();
    cookieJar.setCookie(request.cookie(cookieString));

    var requestOptions = {
      url: odoo_oauth_url,
      jar: cookieJar,
      followRedirect: false
    };

    request(requestOptions, function (error, response) {
      _self.sessionIdResponseHandler(error, response, resultCallback);
    });

  },

  /* Session Context related functions */
  getSessionInfo: function (id_token, session_id, resultCallback) {
    console.log('id_token: ' + id_token);
    console.log('session_id: ' + session_id);
    var _self = this;
    var params = {};

    var client = jaysonHelper.getJaysonClient(id_token, session_id, constants.URL_SESSION_INFO);

    client.request('call', params, function (error, response) {
      console.log(response);
      if (error) {
        var errorJSON = constants.ERRORS.JAYSON_ERROR;
        errorJSON['message'] = error.toString();
        debug('Error getting session info: ' + error.toString());
        return resultCallback(error);
      } else {
        return resultCallback(null, response.result);
      }
    });
  }
};

module.exports = AuthHelper;
