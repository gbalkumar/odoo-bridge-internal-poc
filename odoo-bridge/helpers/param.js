var defined = require('./undefined').defined;

var ParamHelper = {

  validateParam: function(request, paramName) {
    var paramValue = request.params[paramName];
    return (defined(paramValue) && paramValue.trim().length !== 0);
  },

  validateQuery: function(request, queryName) {
    var queryValue = request.query[queryName];
    return (defined(queryValue) && queryValue.trim().length !== 0);
  },

  getParamValue: function(request, paramName) {
    return (this.validateParam(request, paramName)) ? request.params[paramName] : undefined;
  },

  getQueryValue: function(request, queryName) {
    return (this.validateQuery(request, queryName)) ? request.query[queryName] : undefined;
  },

  getRequestBodyContent: function(request) {
    return (typeof request.body === 'object') ? request.body : undefined;
  },

  getIntegerQueryValue: function(request, queryName) {
    return parseInt(this.getQueryValue(request, queryName));
  }

}

module.exports = ParamHelper;
