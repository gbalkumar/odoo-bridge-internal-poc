var defined = function (variable) {
  return typeof (variable) !== 'undefined' && variable !== null;
};

var notEmpty = function (variable) {
  return defined(variable) && variable.trim().length !== 0;
};

module.exports = {
  defined: defined,
  notEmpty: notEmpty
}
