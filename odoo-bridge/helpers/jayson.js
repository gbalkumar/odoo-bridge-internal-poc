var jayson = require('jayson');
var constants = require('./constants');
var util = require('util');

var JaysonHelper = {
  getJaysonClient: function (id_token, session_id, request_path) {
    var client;
    if (constants.JAYSON.PORT === 443) {
      client = jayson.client.https({
        host: constants.JAYSON.HOST,
        port: constants.JAYSON.PORT,
        path: request_path,
        headers: {
          'Cookie': util.format('%s=%s;%s=%s',
            constants.COOKIES.ID_TOKEN, id_token,
            constants.COOKIES.SESSION_ID, session_id)
        }
      });
    } else {
      client = jayson.client.http({
        host: constants.JAYSON.HOST,
        port: constants.JAYSON.PORT,
        path: request_path,
        headers: {
          'Cookie': util.format('%s=%s;%s=%s',
            constants.COOKIES.ID_TOKEN, id_token,
            constants.COOKIES.SESSION_ID, session_id)
        }
      });
    }
    return client;
  },

  getErrorObject: function (message) {
    var object = JSON.parse(JSON.stringify(constants.ERRORS.JAYSON_ERROR));
    object['message'] = message.toString();
    return object;
  },

  callJaysonRequest: function (jaysonClient, params, callback) {
    jaysonClient.request('call', params, function (error, jaysonResponse) {
      if (error) {
        return callback(error);
      } else if (jaysonResponse.error) {
        if (jaysonResponse.error.data.name === constants.ODOO_SESSION_EXPIRED) {
          return callback('This session was expired');
        } else if (jaysonResponse.error.data.name === constants.ODOO_ACCESS_ERROR) {
          return callback('Access Error');
        } else if (jaysonResponse.error.data.name === constants.ODOO_NOT_FOUND) {
          return callback('Not Found');
        } else {
          return callback(jaysonResponse.error.data.name + '; ' + jaysonResponse.error.data.message);
        }
      } else {
        return callback(null, jaysonResponse.result);
      }
    });
  }
};


module.exports = JaysonHelper;
