var define = require("node-constants")(exports);

define({
  // Give the IP of your local docker-machine running Odoo
  URL_ODOO_OAUTH_EP: 'http://192.168.99.100:8069/auth_oauth/signin?id_token=%s&access_token=%s&token_type=Bearer&state=%7B%22p%22%3A%204%2C%20%22d%22%3A%20%22maestro%22%7D',
  URL_SESSION_INFO: '/web/session/get_session_info',
  URL_PATH_GET: '/web/dataset/search_read',
  URL_PATH_POST: '/web/dataset/call_kw/%s/create', // modelName
  URL_PATH_PATCH: '/web/dataset/call_kw/%s/write', // modelName
  URL_PATH_DELETE: '/web/dataset/call_kw/%s/unlink', // modelName
  URL_INVOKE_ACTION: '/web/dataset/call_kw/%s/%s',
  URL_INVOKE_ACTION_DIRECT: '/%s/%s',

  XAUTH_HEADERS: {
    ID_TOKEN: 'x_auth_id_token',
    ACCESS_TOKEN: 'x_auth_access_token',
    SESSION_ID: 'x_auth_session_id'
  },

  COOKIES: {
    ID_TOKEN: 'id_token',
    ACCESS_TOKEN: 'access_token',
    SESSION_ID: 'session_id'
  },

  // Give the IP of your local docker-machine running Odoo
  JAYSON: {
    HOST: '192.168.99.100',
    PORT: 8069
  },

  ERRORS: {
    AUTH_ERROR: {
      'result': false,
      'message': 'You need to be authenticated',
      'code': 'E_AUTH_ERROR'
    },
    SERVER_ERROR: {
      'result': false,
      'message': 'Internal Server Error',
      'code': 'E_SERVER_ERROR'
    },
    TOKENS_NOT_SET: {
      'result': false,
      'message': 'id_token and access_token not set',
      'code': 'E_NO_TOKENS'
    },
    INVALID_TOKENS: {
      'result': false,
      'message': 'Invalid Tokens',
      'code': 'E_INVALID_TOKENS'
    },
    OAUTH_ERROR: {
      'result': false,
      'message': 'Internal Server Error',
      'code': 'E_OAUTH_ERROR'
    },
    JAYSON_ERROR: {
      'result': false,
      'message': '', // filled dynamically
      'code': 'E_JSONRPC_ERROR'
    },
    INVALID_RECORD_ID: {
      'result': false,
      'message': 'Invalid Record ID - should be interger',
      'code': 'E_INVALID_RECORD_ID'
    }
  },

  ODOO_NOT_FOUND: 'werkzeug.exceptions.NotFound',
  ODOO_SESSION_EXPIRED: 'openerp.http.SessionExpiredException',
  ODOO_ACCESS_ERROR: 'openerp.exceptions.AccessError'

});
