## Clone this repository and in odoo-bridge folder run

1. npm install
2. npm start (this will start the server on locahost:3000)

## Prerequisites:
You need a Redis instance, which will be used by this bridge to register sessions. After setting it up, change the `host` and `port` in the `odoo-bridge/helpers/constants.js`.
